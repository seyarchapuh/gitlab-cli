#!/bin/sh

yarn build;
yarn version --patch;
git add .;
echo "Enter commit message:"
read EVENT
git commit -m "${EVENT}";
git tag -m "${EVENT}";
