import * as https from 'https'
import * as queryString from 'querystring'
import { ParsedUrlQueryInput } from 'querystring'
import { logger } from './logger'

export interface IRepoSettings {
  user: string
  repo: string
  pullsPerPage: number
  githubToken: string
}

interface IChangelog {
  commits: ICommit[]
}

export interface IReleaseResult {
  result: 'ok' | 'error'
  message: string
}

interface ICommit {
  commit: {
    url: string
    author: {
      name: string
    }
    message: string
  }
}

interface IUser {
  login: string
  id: number
  node_id: string
  avatar_url: string
  gravatar_id: ''
  url: string
  html_url: string
  followers_url: string
  following_url: string
  gists_url: string
  starred_url: string
  subscriptions_url: string
  organizations_url: string
  repos_url: string
  events_url: string
  received_events_url: string
  type: string
  site_admin: boolean
}

export interface IReleaseOptions {
  changelog: string
  version: string
  branch: string
}

interface IReleaseResponse {
  url: string
  html_url: string
  assets_url: string
  upload_url: string
  tarball_url: string
  zipball_url: string
  id: number
  node_id: string
  tag_name: string
  target_commitish: string
  name: string
  body: string
  draft: false
  prerelease: false
  created_at: string
  published_at: string
  author: IUser
}
interface IComment {
  path: string
  position: number
  body: string
}

export interface ISubmitReviewPut {
  review_id?: string
  body?: string
  event: 'APPROVE' | 'COMMENT' | 'REQUEST_CHANGES'
}

export interface IReviewPut {
  body?: string
  event: string
  comments?: IComment[]
}

export interface IMergeOptions {
  commit_title?: string
  commit_message?: string
  sha?: string
  merge_method?: 'merge' | 'squash' | 'rebase'
}

interface IReviewResponse {
  id: number
  node_id: string
  user: IUser
  body: string
  state: 'CHANGES_REQUESTED' | 'APPROVED'
  html_url: string
  pull_request_url: string
  _links: {
    html: {
      href: string
    }
    pull_request: {
      href: string
    }
  }
  submitted_at: string
  commit_id: string
  author_association: string
}

interface IApproveResponse {
  id: number
  node_id: string
  user: IUser
  body: string
  state: 'CHANGES_REQUESTED' | 'APPROVED'
  html_url: string
  pull_request_url: string
  _links: {
    html: {
      href: string
    }
    pull_request: {
      href: string
    }
  }
  submitted_at: string
  commit_id: string
  author_association: string
}

interface IMergeResponse {
  sha: string
  merged: boolean
  message: string
}

interface IPullParams extends ParsedUrlQueryInput {
  state: 'open, closed, or all'
  head: string
  base: string
  sort: string
  direction: 'asc' | 'desc'
  per_page: number
  page: number
}

export interface IPull {
  url: string
  id: number
  node_id: string
  html_url: string
  diff_url: string
  patch_url: string
  issue_url: string
  commits_url: string
  review_comments_url: string
  review_comment_url: string
  comments_url: string
  statuses_url: string
  number: number
  state: string
  locked: boolean
  title: string
  user: IUser
  body: string
  labels: [
    {
      id: number
      node_id: string
      url: string
      name: string
      description: string
      color: string
      default: boolean
    }
  ]
  milestone: {
    url: string
    html_url: string
    labels_url: string
    id: number
    node_id: string
    number: number
    state: string
    title: string
    description: string
    creator: IUser
    open_issues: number
    closed_issues: number
    created_at: string
    updated_at: string
    closed_at: string
    due_on: string
  }
  active_lock_reason: string
  created_at: string
  updated_at: string
  closed_at: string
  merged_at: string
  merge_commit_sha: string
  assignee: IUser
  assignees: [IUser, IUser]
  requested_reviewers: [IUser]
  requested_teams: [
    {
      id: number
      node_id: string
      url: string
      html_url: string
      name: string
      slug: string
      description: string
      privacy: string
      permission: string
      members_url: string
      repositories_url: string
    }
  ]
  head: {
    label: string
    ref: string
    sha: string
    user: IUser
    repo: {
      id: number
      node_id: string
      name: string
      full_name: string
      owner: IUser
      private: boolean
      html_url: string
      description: string
      fork: boolean
      url: string
      archive_url: string
      assignees_url: string
      blobs_url: string
      branches_url: string
      collaborators_url: string
      comments_url: string
      commits_url: string
      compare_url: string
      contents_url: string
      contributors_url: string
      deployments_url: string
      downloads_url: string
      events_url: string
      forks_url: string
      git_commits_url: string
      git_refs_url: string
      git_tags_url: string
      git_url: string
      issue_comment_url: string
      issue_events_url: string
      issues_url: string
      keys_url: string
      labels_url: string
      languages_url: string
      merges_url: string
      milestones_url: string
      notifications_url: string
      pulls_url: string
      releases_url: string
      ssh_url: string
      stargazers_url: string
      statuses_url: string
      subscribers_url: string
      subscription_url: string
      tags_url: string
      teams_url: string
      trees_url: string
      clone_url: string
      mirror_url: string
      hooks_url: string
      svn_url: string
      homepage: string
      language: null
      forks_count: number
      stargazers_count: number
      watchers_count: number
      size: number
      default_branch: string
      open_issues_count: number
      is_template: boolean
      topics: string[]
      has_issues: boolean
      has_projects: boolean
      has_wiki: boolean
      has_pages: boolean
      has_downloads: boolean
      archived: boolean
      disabled: boolean
      visibility: string
      pushed_at: string
      created_at: string
      updated_at: string
      permissions: {
        admin: boolean
        push: boolean
        pull: boolean
      }
      allow_rebase_merge: boolean
      template_repository: null
      temp_clone_token: string
      allow_squash_merge: boolean
      delete_branch_on_merge: boolean
      allow_merge_commit: boolean
      subscribers_count: number
      network_count: number
      license: {
        key: string
        name: string
        url: string
        spdx_id: string
        node_id: string
        html_url: string
      }
      forks: number
      open_issues: number
      watchers: 1
    }
  }
  base: {
    label: string
    ref: string
    sha: string
    user: IUser
    repo: {
      id: number
      node_id: string
      name: string
      full_name: string
      owner: IUser
      private: boolean
      html_url: string
      description: string
      fork: boolean
      url: string
      archive_url: string
      assignees_url: string
      blobs_url: string
      branches_url: string
      collaborators_url: string
      comments_url: string
      commits_url: string
      compare_url: string
      contents_url: string
      contributors_url: string
      deployments_url: string
      downloads_url: string
      events_url: string
      forks_url: string
      git_commits_url: string
      git_refs_url: string
      git_tags_url: string
      git_url: string
      issue_comment_url: string
      issue_events_url: string
      issues_url: string
      keys_url: string
      labels_url: string
      languages_url: string
      merges_url: string
      milestones_url: string
      notifications_url: string
      pulls_url: string
      releases_url: string
      ssh_url: string
      stargazers_url: string
      statuses_url: string
      subscribers_url: string
      subscription_url: string
      tags_url: string
      teams_url: string
      trees_url: string
      clone_url: string
      mirror_url: string
      hooks_url: string
      svn_url: string
      homepage: string
      language: null
      forks_count: number
      stargazers_count: number
      watchers_count: number
      size: number
      default_branch: string
      open_issues_count: number
      is_template: boolean
      topics: string[]
      has_issues: boolean
      has_projects: boolean
      has_wiki: boolean
      has_pages: boolean
      has_downloads: boolean
      archived: boolean
      disabled: boolean
      visibility: string
      pushed_at: string
      created_at: string
      updated_at: string
      permissions: {
        admin: boolean
        push: boolean
        pull: boolean
      }
      allow_rebase_merge: boolean
      template_repository: null
      temp_clone_token: string
      allow_squash_merge: boolean
      delete_branch_on_merge: boolean
      allow_merge_commit: boolean
      subscribers_count: number
      network_count: number
      license: {
        key: string
        name: string
        url: string
        spdx_id: string
        node_id: string
        html_url: string
      }
      forks: number
      open_issues: number
      watchers: 1
    }
  }
  _links: {
    self: {
      href: string
    }
    html: {
      href: string
    }
    issue: {
      href: string
    }
    comments: {
      href: string
    }
    review_comments: {
      href: string
    }
    review_comment: {
      href: string
    }
    commits: {
      href: string
    }
    statuses: {
      href: string
    }
  }
  author_association: string
  draft: boolean
}

const API_URL = 'api.github.com'

class Realizer {
  private readonly user: string
  private readonly repo: string
  private readonly githubToken: string

  constructor({ user, repo, githubToken }: IRepoSettings) {
    this.user = user
    this.repo = repo
    this.githubToken = githubToken
  }

  makeRelease = async (
    isProduction: boolean,
    { version, changelog, branch }: IReleaseOptions
  ): Promise<IReleaseResult> => {
    const name = version.replace('release', 'Release')
    const target = branch || 'release'
    const data = JSON.stringify({
      tag_name: version,
      target_commitish: target,
      name: name,
      body: changelog,
      draft: false,
      prerelease: false,
    }).replace("'", "'")

    logger.debug(
      `Create ${name} for repo:${this.user}/${this.repo} branch:${target}`
    )

    const options = {
      method: 'POST',
      data,
    }

    if (isProduction) {
      logger.info(JSON.stringify(options, null, 2))

      const message = await this.requestGithub<IReleaseResponse>(
        '/releases',
        options
      )
      logger.debug(JSON.stringify(message))

      return {
        result: 'ok',
        message: `Success ${String(message.created_at)} ${message.tag_name}`,
      }
    } else {
      const message = `Fake request github for release
      \`/repos/${this.user}/${this.repo}/releases\`,
      body = \`\`\`\n${JSON.stringify(options)}\`\`\``

      logger.info(message)
      return { result: 'ok', message }
    }
  }

  getLatestVersion = (latestRelease: string, changelog: string): string => {
    return latestRelease.replace(
      /\.(\d+)\.(\d+)$/,
      (matches, minor: string, patch: string) => {
        if (changelog.includes('chore') || changelog.includes('feat')) {
          minor = String(Number(minor) + 1)
          patch = '0'
        } else {
          patch = String(Number(patch) + 1)
        }

        return `.${minor}.${patch}`
      }
    )
  }

  getChangelog = async (
    previousRelease: string,
    latestRelease: string
  ): Promise<string> => {
    const compare = await this.requestGithub<IChangelog>(
      `/compare/${previousRelease}...${latestRelease}`
    )

    return compare.commits
      .map(({ commit }) => {
        const { message, author } = commit
        const mShort = message.slice(0, message.indexOf('\n\n') + 1)
        const m = mShort || message

        return `* ${m.replace(/'/g, '').replace(/"/g, '')} (${String(
          author.name
        )})`
      })
      .join('\n')
  }

  /**
   * @returns {Promise<{name: String, body: String}>}
   */
  getReleaseNotes = async (tagName: string): Promise<any> => {
    const response = await this.requestGithub<{ body: string }>(
      '/releases/generate-notes',
      {
        method: 'POST',
        data: JSON.stringify({
          tag_name: tagName,
        }),
      }
    )

    logger.debug(`Release notes ${String(response.body)}`)
    return response
  }

  getPulls = async (params: Partial<IPullParams> = {}): Promise<IPull[]> => {
    try {
      return this.requestGithub<IPull[]>(
        `/pulls?${queryString.stringify(params)}`
      )
    } catch (err) {
      logger.error(err)
      throw err
    }
  }

  findPullRequest = async (branch: string): Promise<IPull | undefined> => {
    const pulls = await this.requestGithub<IPull[]>('/pulls?state=open')
    return pulls.find(pull => pull.head.ref === branch)
  }

  mergePull = async (
    pull: IPull,
    options?: IMergeOptions
  ): Promise<IMergeResponse> => {
    const data = {
      merge_method: 'squash',
      ...options,
    }

    try {
      return this.requestGithub<IMergeResponse>(
        `/pulls/${String(pull.number)}/merge`,
        {
          method: 'PUT',
          data: JSON.stringify(data),
        }
      )
    } catch (err) {
      logger.error(err)
      throw err
    }
  }

  approvePull = async (
    pull: IPull,
    options?: IReviewPut
  ): Promise<IApproveResponse> => {
    // APPROVE, REQUEST_CHANGES, COMMENT
    const reviewData: IReviewPut = {
      event: 'APPROVE',
      ...options,
    }

    try {
      const review = await this.requestGithub<IReviewResponse>(
        `/pulls/${String(pull.number)}/reviews`,
        {
          method: 'POST',
          data: JSON.stringify(reviewData),
        }
      )

      if (!review?.id) {
        throw Error(
          `Review not submitted ${JSON.stringify(review)} ${JSON.stringify(
            reviewData
          )}`
        )
      }

      const submitData: ISubmitReviewPut = {
        // body: 'Here is the body for the review.',
        event: 'APPROVE',
      }

      return this.requestGithub<IApproveResponse>(
        `/pulls/${String(pull.number)}/reviews/${String(review.id)}/events/`,
        {
          method: 'POST',
          data: JSON.stringify(submitData),
        }
      )
    } catch (err) {
      logger.error(err)
      throw err
    }
  }

  private async requestGithub<T extends unknown>(
    url: string,
    options: Partial<https.RequestOptions & { data: {} }> = {}
  ): Promise<T> {
    const fullUrl = `/repos/${this.user}/${this.repo}${url}`
    options = {
      hostname: API_URL,
      port: 443,
      path: fullUrl,
      method: 'GET',
      headers: {
        'User-Agent': 'curl/7.37.0',
        'Content-Type': 'application/json',
        Accept: 'application/vnd.github.v3+json',
        Authorization: `token ${this.githubToken}`,
      },
      ...options,
    }

    logger.debug(`Request github ${String(options.method)}: ${fullUrl}`)

    return new Promise((resolve, reject) => {
      const req = https.request(options, res => {
        res.setEncoding('utf8')
        let body = ''
        res.on('error', (err: string) => {
          reject(err)
        })
        res.on('data', (data: string) => {
          body += data
        })
        res.on('end', () => {
          let response
          try {
            response = JSON.parse(body)
          } catch (e) {
            reject(e)
          }

          resolve(response)
        })
      })
      options.data && req.write(options.data)
      req.end()
    })
  }

  getLatestRelease = async (): Promise<IReleaseResponse> => {
    const body = await this.requestGithub<IReleaseResponse>('/releases/latest')
    logger.debug(`last release is ${String(body.tag_name)}`)
    return body
  }
}

export { Realizer }
