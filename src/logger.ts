import * as pino from 'pino'

const logger = pino({
  level: process.env.LOG_LEVEL ?? 'warn',
  prettyPrint: process.env.NODE_ENV === 'development',
})

export { logger }
