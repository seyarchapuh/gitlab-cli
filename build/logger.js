'use strict'
var _a
Object.defineProperty(exports, '__esModule', { value: true })
exports.logger = void 0
const pino = require('pino')
const logger = pino({
  level: (_a = process.env.LOG_LEVEL) !== null && _a !== void 0 ? _a : 'warn',
  prettyPrint: process.env.NODE_ENV === 'development',
})
exports.logger = logger
