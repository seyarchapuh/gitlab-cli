/// <reference types="node" />
import { ParsedUrlQueryInput } from 'querystring'
export interface IRepoSettings {
  user: string
  repo: string
  pullsPerPage: number
  githubToken: string
}
export interface IReleaseResult {
  result: 'ok' | 'error'
  message: string
}
interface IUser {
  login: string
  id: number
  node_id: string
  avatar_url: string
  gravatar_id: ''
  url: string
  html_url: string
  followers_url: string
  following_url: string
  gists_url: string
  starred_url: string
  subscriptions_url: string
  organizations_url: string
  repos_url: string
  events_url: string
  received_events_url: string
  type: string
  site_admin: boolean
}
export interface IReleaseOptions {
  changelog: string
  version: string
  branch: string
}
interface IReleaseResponse {
  url: string
  html_url: string
  assets_url: string
  upload_url: string
  tarball_url: string
  zipball_url: string
  id: number
  node_id: string
  tag_name: string
  target_commitish: string
  name: string
  body: string
  draft: false
  prerelease: false
  created_at: string
  published_at: string
  author: IUser
}
interface IComment {
  path: string
  position: number
  body: string
}
export interface ISubmitReviewPut {
  review_id?: string
  body?: string
  event: 'APPROVE' | 'COMMENT' | 'REQUEST_CHANGES'
}
export interface IReviewPut {
  body?: string
  event: string
  comments?: IComment[]
}
export interface IMergeOptions {
  commit_title?: string
  commit_message?: string
  sha?: string
  merge_method?: 'merge' | 'squash' | 'rebase'
}
interface IApproveResponse {
  id: number
  node_id: string
  user: IUser
  body: string
  state: 'CHANGES_REQUESTED' | 'APPROVED'
  html_url: string
  pull_request_url: string
  _links: {
    html: {
      href: string
    }
    pull_request: {
      href: string
    }
  }
  submitted_at: string
  commit_id: string
  author_association: string
}
interface IMergeResponse {
  sha: string
  merged: boolean
  message: string
}
interface IPullParams extends ParsedUrlQueryInput {
  state: 'open, closed, or all'
  head: string
  base: string
  sort: string
  direction: 'asc' | 'desc'
  per_page: number
  page: number
}
export interface IPull {
  url: string
  id: number
  node_id: string
  html_url: string
  diff_url: string
  patch_url: string
  issue_url: string
  commits_url: string
  review_comments_url: string
  review_comment_url: string
  comments_url: string
  statuses_url: string
  number: number
  state: string
  locked: boolean
  title: string
  user: IUser
  body: string
  labels: [
    {
      id: number
      node_id: string
      url: string
      name: string
      description: string
      color: string
      default: boolean
    }
  ]
  milestone: {
    url: string
    html_url: string
    labels_url: string
    id: number
    node_id: string
    number: number
    state: string
    title: string
    description: string
    creator: IUser
    open_issues: number
    closed_issues: number
    created_at: string
    updated_at: string
    closed_at: string
    due_on: string
  }
  active_lock_reason: string
  created_at: string
  updated_at: string
  closed_at: string
  merged_at: string
  merge_commit_sha: string
  assignee: IUser
  assignees: [IUser, IUser]
  requested_reviewers: [IUser]
  requested_teams: [
    {
      id: number
      node_id: string
      url: string
      html_url: string
      name: string
      slug: string
      description: string
      privacy: string
      permission: string
      members_url: string
      repositories_url: string
    }
  ]
  head: {
    label: string
    ref: string
    sha: string
    user: IUser
    repo: {
      id: number
      node_id: string
      name: string
      full_name: string
      owner: IUser
      private: boolean
      html_url: string
      description: string
      fork: boolean
      url: string
      archive_url: string
      assignees_url: string
      blobs_url: string
      branches_url: string
      collaborators_url: string
      comments_url: string
      commits_url: string
      compare_url: string
      contents_url: string
      contributors_url: string
      deployments_url: string
      downloads_url: string
      events_url: string
      forks_url: string
      git_commits_url: string
      git_refs_url: string
      git_tags_url: string
      git_url: string
      issue_comment_url: string
      issue_events_url: string
      issues_url: string
      keys_url: string
      labels_url: string
      languages_url: string
      merges_url: string
      milestones_url: string
      notifications_url: string
      pulls_url: string
      releases_url: string
      ssh_url: string
      stargazers_url: string
      statuses_url: string
      subscribers_url: string
      subscription_url: string
      tags_url: string
      teams_url: string
      trees_url: string
      clone_url: string
      mirror_url: string
      hooks_url: string
      svn_url: string
      homepage: string
      language: null
      forks_count: number
      stargazers_count: number
      watchers_count: number
      size: number
      default_branch: string
      open_issues_count: number
      is_template: boolean
      topics: string[]
      has_issues: boolean
      has_projects: boolean
      has_wiki: boolean
      has_pages: boolean
      has_downloads: boolean
      archived: boolean
      disabled: boolean
      visibility: string
      pushed_at: string
      created_at: string
      updated_at: string
      permissions: {
        admin: boolean
        push: boolean
        pull: boolean
      }
      allow_rebase_merge: boolean
      template_repository: null
      temp_clone_token: string
      allow_squash_merge: boolean
      delete_branch_on_merge: boolean
      allow_merge_commit: boolean
      subscribers_count: number
      network_count: number
      license: {
        key: string
        name: string
        url: string
        spdx_id: string
        node_id: string
        html_url: string
      }
      forks: number
      open_issues: number
      watchers: 1
    }
  }
  base: {
    label: string
    ref: string
    sha: string
    user: IUser
    repo: {
      id: number
      node_id: string
      name: string
      full_name: string
      owner: IUser
      private: boolean
      html_url: string
      description: string
      fork: boolean
      url: string
      archive_url: string
      assignees_url: string
      blobs_url: string
      branches_url: string
      collaborators_url: string
      comments_url: string
      commits_url: string
      compare_url: string
      contents_url: string
      contributors_url: string
      deployments_url: string
      downloads_url: string
      events_url: string
      forks_url: string
      git_commits_url: string
      git_refs_url: string
      git_tags_url: string
      git_url: string
      issue_comment_url: string
      issue_events_url: string
      issues_url: string
      keys_url: string
      labels_url: string
      languages_url: string
      merges_url: string
      milestones_url: string
      notifications_url: string
      pulls_url: string
      releases_url: string
      ssh_url: string
      stargazers_url: string
      statuses_url: string
      subscribers_url: string
      subscription_url: string
      tags_url: string
      teams_url: string
      trees_url: string
      clone_url: string
      mirror_url: string
      hooks_url: string
      svn_url: string
      homepage: string
      language: null
      forks_count: number
      stargazers_count: number
      watchers_count: number
      size: number
      default_branch: string
      open_issues_count: number
      is_template: boolean
      topics: string[]
      has_issues: boolean
      has_projects: boolean
      has_wiki: boolean
      has_pages: boolean
      has_downloads: boolean
      archived: boolean
      disabled: boolean
      visibility: string
      pushed_at: string
      created_at: string
      updated_at: string
      permissions: {
        admin: boolean
        push: boolean
        pull: boolean
      }
      allow_rebase_merge: boolean
      template_repository: null
      temp_clone_token: string
      allow_squash_merge: boolean
      delete_branch_on_merge: boolean
      allow_merge_commit: boolean
      subscribers_count: number
      network_count: number
      license: {
        key: string
        name: string
        url: string
        spdx_id: string
        node_id: string
        html_url: string
      }
      forks: number
      open_issues: number
      watchers: 1
    }
  }
  _links: {
    self: {
      href: string
    }
    html: {
      href: string
    }
    issue: {
      href: string
    }
    comments: {
      href: string
    }
    review_comments: {
      href: string
    }
    review_comment: {
      href: string
    }
    commits: {
      href: string
    }
    statuses: {
      href: string
    }
  }
  author_association: string
  draft: boolean
}
declare class Realizer {
  private readonly user
  private readonly repo
  private readonly githubToken
  constructor({ user, repo, githubToken }: IRepoSettings)
  makeRelease: (
    isProduction: boolean,
    { version, changelog, branch }: IReleaseOptions
  ) => Promise<IReleaseResult>
  getLatestVersion: (latestRelease: string, changelog: string) => string
  getChangelog: (
    previousRelease: string,
    latestRelease: string
  ) => Promise<string>
  getReleaseNotes: (tagName: string) => Promise<any>
  getPulls: (params?: Partial<IPullParams>) => Promise<IPull[]>
  findPullRequest: (branch: string) => Promise<IPull | undefined>
  mergePull: (
    pull: IPull,
    options?: IMergeOptions | undefined
  ) => Promise<IMergeResponse>
  approvePull: (
    pull: IPull,
    options?: IReviewPut | undefined
  ) => Promise<IApproveResponse>
  private requestGithub
  getLatestRelease: () => Promise<IReleaseResponse>
}
export { Realizer }
