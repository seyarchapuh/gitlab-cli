'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.Realizer = void 0
const tslib_1 = require('tslib')
const https = require('https')
const queryString = require('querystring')
const logger_1 = require('./logger')
const API_URL = 'api.github.com'
class Realizer {
  constructor({ user, repo, githubToken }) {
    this.makeRelease = (isProduction, { version, changelog, branch }) =>
      tslib_1.__awaiter(this, void 0, void 0, function*() {
        const name = version.replace('release', 'Release')
        const target = branch || 'release'
        const data = JSON.stringify({
          tag_name: version,
          target_commitish: target,
          name: name,
          body: changelog,
          draft: false,
          prerelease: false,
        }).replace("'", "'")
        logger_1.logger.debug(
          `Create ${name} for repo:${this.user}/${this.repo} branch:${target}`
        )
        const options = {
          method: 'POST',
          data,
        }
        if (isProduction) {
          logger_1.logger.info(JSON.stringify(options, null, 2))
          const message = yield this.requestGithub('/releases', options)
          logger_1.logger.debug(JSON.stringify(message))
          return {
            result: 'ok',
            message: `Success ${String(message.created_at)} ${
              message.tag_name
            }`,
          }
        } else {
          const message = `Fake request github for release
      \`/repos/${this.user}/${this.repo}/releases\`,
      body = \`\`\`\n${JSON.stringify(options)}\`\`\``
          logger_1.logger.info(message)
          return { result: 'ok', message }
        }
      })
    this.getLatestVersion = (latestRelease, changelog) => {
      return latestRelease.replace(
        /\.(\d+)\.(\d+)$/,
        (matches, minor, patch) => {
          if (changelog.includes('chore') || changelog.includes('feat')) {
            minor = String(Number(minor) + 1)
            patch = '0'
          } else {
            patch = String(Number(patch) + 1)
          }
          return `.${minor}.${patch}`
        }
      )
    }
    this.getChangelog = (previousRelease, latestRelease) =>
      tslib_1.__awaiter(this, void 0, void 0, function*() {
        const compare = yield this.requestGithub(
          `/compare/${previousRelease}...${latestRelease}`
        )
        return compare.commits
          .map(({ commit }) => {
            const { message, author } = commit
            const mShort = message.slice(0, message.indexOf('\n\n') + 1)
            const m = mShort || message
            return `* ${m.replace(/'/g, '').replace(/"/g, '')} (${String(
              author.name
            )})`
          })
          .join('\n')
      })
    this.getReleaseNotes = tagName =>
      tslib_1.__awaiter(this, void 0, void 0, function*() {
        const response = yield this.requestGithub('/releases/generate-notes', {
          method: 'POST',
          data: JSON.stringify({
            tag_name: tagName,
          }),
        })
        logger_1.logger.debug(`Release notes ${String(response.body)}`)
        return response
      })
    this.getPulls = (params = {}) =>
      tslib_1.__awaiter(this, void 0, void 0, function*() {
        try {
          return this.requestGithub(`/pulls?${queryString.stringify(params)}`)
        } catch (err) {
          logger_1.logger.error(err)
          throw err
        }
      })
    this.findPullRequest = branch =>
      tslib_1.__awaiter(this, void 0, void 0, function*() {
        const pulls = yield this.requestGithub('/pulls?state=open')
        return pulls.find(pull => pull.head.ref === branch)
      })
    this.mergePull = (pull, options) =>
      tslib_1.__awaiter(this, void 0, void 0, function*() {
        const data = Object.assign({ merge_method: 'squash' }, options)
        try {
          return this.requestGithub(`/pulls/${String(pull.number)}/merge`, {
            method: 'PUT',
            data: JSON.stringify(data),
          })
        } catch (err) {
          logger_1.logger.error(err)
          throw err
        }
      })
    this.approvePull = (pull, options) =>
      tslib_1.__awaiter(this, void 0, void 0, function*() {
        const reviewData = Object.assign({ event: 'APPROVE' }, options)
        try {
          const review = yield this.requestGithub(
            `/pulls/${String(pull.number)}/reviews`,
            {
              method: 'POST',
              data: JSON.stringify(reviewData),
            }
          )
          if (!(review === null || review === void 0 ? void 0 : review.id)) {
            throw Error(
              `Review not submitted ${JSON.stringify(review)} ${JSON.stringify(
                reviewData
              )}`
            )
          }
          const submitData = {
            event: 'APPROVE',
          }
          return this.requestGithub(
            `/pulls/${String(pull.number)}/reviews/${String(
              review.id
            )}/events/`,
            {
              method: 'POST',
              data: JSON.stringify(submitData),
            }
          )
        } catch (err) {
          logger_1.logger.error(err)
          throw err
        }
      })
    this.getLatestRelease = () =>
      tslib_1.__awaiter(this, void 0, void 0, function*() {
        const body = yield this.requestGithub('/releases/latest')
        logger_1.logger.debug(`last release is ${String(body.tag_name)}`)
        return body
      })
    this.user = user
    this.repo = repo
    this.githubToken = githubToken
  }
  requestGithub(url, options = {}) {
    return tslib_1.__awaiter(this, void 0, void 0, function*() {
      const fullUrl = `/repos/${this.user}/${this.repo}${url}`
      options = Object.assign(
        {
          hostname: API_URL,
          port: 443,
          path: fullUrl,
          method: 'GET',
          headers: {
            'User-Agent': 'curl/7.37.0',
            'Content-Type': 'application/json',
            Accept: 'application/vnd.github.v3+json',
            Authorization: `token ${this.githubToken}`,
          },
        },
        options
      )
      logger_1.logger.debug(
        `Request github ${String(options.method)}: ${fullUrl}`
      )
      return new Promise((resolve, reject) => {
        const req = https.request(options, res => {
          res.setEncoding('utf8')
          let body = ''
          res.on('error', err => {
            reject(err)
          })
          res.on('data', data => {
            body += data
          })
          res.on('end', () => {
            let response
            try {
              response = JSON.parse(body)
            } catch (e) {
              reject(e)
            }
            resolve(response)
          })
        })
        options.data && req.write(options.data)
        req.end()
      })
    })
  }
}
exports.Realizer = Realizer
